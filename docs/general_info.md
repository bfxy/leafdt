# What is Elevator

An elevator is a vertical transportation device designed to move people
between the floors of a building. Elevators are commonly
built inside middle to high-rise buildings where walking the stairs
up to the top demands a physical challenge. Modern elevators
are powered by electric motors that allow passengers to travel more
than ten floors up or down in a matter of several seconds.

## Purpose of use

An elevator is the most efficient way of vertical transportation within the
building. Elevators can carry people and cargo to as high as 100
floors in the tallest buildings in the world. Elevators are easily
controlled and using them requires little to no technical knowledge. Traveling on a modern elevator is a completely safe procedure.

## Design and function

![](img/1.jpg)

Modern elevator is a complicated mechanism based on the simple
physical principle. The design of a modern elevator includes:

-   Passenger carriage
-   Electric motor
-   Control system

### Carriage

A passenger carriage travels inside a shaft. A shaft is usually
engineered prior to the building construction. A shaft is designed to
have slightly wider dimensions than the carriage to allow installing
emergency equipment and performing maintenance. A shaft typically
stretches to the full height of a building.

A shaft has a pair of doors on each floor to prevent accidents.
A carriage has its own set of doors that only operate synchronously with
the shaft doors.

A carriage allows only limited number of people to travel simultaneously. Any overweight is harmful to the mechanism of an elevator. Traveling overweight is prevented by an automatic control system that will refuse to
close the doors and continue the travel.

### Motor

An electric motor is usually installed at the very top of a shaft. A
carriage is connected to a counterweight via a steel rope mounted over a
a pulley. A pulley is driven by an electric motor and provides
traction to a carriage with a counterweight. This type of mechanism is
often called a traction elevator.

### Control system and automation

Any modern elevator's operation is controlled by an automatic 
control system. The control system provides several functions:

-   Carries passengers to a specified floor.
-   Opens and closes doors.
-   Triggers emergency mode.
-   Provides emergency devices inside a carriage.

Any passenger carriage features a control panel on one of its inner
walls. The control panel has several buttons designating each floor and
auxiliary buttons that can stop the elevator, hold the doors open, and
call the operator over a speaker in case of emergency. A control panel
may also feature a display to indicate the floor that the elevator
passes by. The same display may appear on each floor of a building outside
the elevator.

Modern elevator carriage is filled with sophisticated
technology to ensure passengers’ safety. This includes weight and
movement sensors, responsive behavior to fire and earthquake alarm,
automatic evacuation algorithms, blocking mechanisms in case of a
breakdown, communication devices for calling help.
