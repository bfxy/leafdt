User Guide for Basic Elevator
====================================

The task was to write a simple and jolly user guide for a common modern
elevator, as if it was just invented and nobody knows how to use it. I
barely used any precise technical vocabulary to keep the
guide genuine and to preserve some artistic tone over it.

All of the information in the guide is fully based on personal
experience with elevators, so it should not be considered a
legitimate reference and hence must not be relied on in a real-life
encounter with an elevator.

Enjoy.
