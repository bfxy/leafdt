# Use Elevator

Using the Elevator is as straightforward and inherent as doing any other
everyday routine. You only have to know you destination floor and to be
a little patient for the elevator to arrive on busy days.

## General safety

As much as a great joy to experience, the elevator is not a place to
play. That being said, using the Elevator requires a
responsibility to acknowledge and a list of rules to follow.

## Do’s and don’ts

The rules of using the elevator are simple and do not require
memorization as most of them will be followed intuitively. However, pay
attention to them for once and especially do not overlook those that are applied if you are accompanied by a child or a baby.

| **DO**                            | **WHY**                           |
|-----------------------------------|-----------------------------------|
| Let out the exiting passengers before entering the carriage. | Basic human decency. |
| <i class="fa fa-child" aria-hidden="true"></i> Enter first and exit last when traveling with a child. | <i class="fa fa-child" aria-hidden="true"></i> A child should not be left alone inside. |
| <i class="fa fa-child" aria-hidden="true"></i> Pick up the baby when traveling with a baby stroller. | <i class="fa fa-child" aria-hidden="true"></i> Traveling with a stroller is difficult, so ensure the baby is never left alone in it. |
| Be careful when loading luggage or large cargo. | Accidental damage to the doors may trigger the emergency mechanism. The Elevator will cease to operate. |
|  Try to create more room inside if the Elevator is loaded. Take off backpack, stand closer to walls. | Respect other peoples’ personal space. Make a ride comfortable for everybody. |

| **DO NOT**                        | **WHY**                              |
|-----------------------------------|--------------------------------------|
| Do not use the elevator if it looks or sounds faulty. | You may be correct. Call the operator for the inspection and use another elevator or the stairs.|
| Do not hold the doors open with your body parts. There is a special button on control panel to hold the doors open. | Stay away from the gap between the doors. |
| Do not try to open doors while traveling.| The emergency mechanism will  block the elevator and it will stop. |
| Do not smoke or set fire inside. | The sensors will trigger the fire alarm and the Elevator will stop. |
| Do not randomly hit buttons on the control panel. | This may damage the electronics in the control panel and lead to a breakdown. |
| Do not jump or lift yourself up inside. | The weight sensor will fail to  detect your presence inside and will stop the elevator. |
| Do not express frustration and outrage towards doors and walls. | Any damage done to the elevator may result in arrest and legal proceeding. Also, the sensors may trigger the earthquake alarm. |

!!! important "Feeling funny?"

    Travel carefully under the influence of alcohol or other substances, or
    the elevator may fly into space and never come back. Jokes aside, be
    responsible and seek for assistance if you feel you may need any.

## Operation

### Call

![](img/call.jpg)

Stand in front of the elevator doors. The display will indicate the
floor where the elevator is currently standing or passing by.

To call the Elevator, press the **Call** button. The button will light up. Be patient as sometimes the elevator will stop on another floor to pick up other passengers on its way.

When the Elevator arrives, you will hear a bell. The doors will open and you will be welcome on board.

### Travel

![](img/floor.jpg)

When the doors open, let out the exiting passengers. Do not rush inside—the sensors between the doors will make sure
you have enough time to board. However, do not hang back to enter the
elevator either—if you wait for too long, the doors will shut anyway. In
this case you have to press the **Call** button again to open the doors.

Once inside, you will see a control panel on the wall. Press the desired
floor's button and wait for the doors to shut. During ascent (or descent)
you may feel a slight dizziness. This is a completely natural human
vestibular system response to the acceleration. The elevator may come in
a very exotic experience for your body at first so give it some time to
adjust.

During the travel the elevator may stop to pick up more passengers. Pay
attention to the display that indicates the floor so you do not miss
your stop.

### Stop or change floor

![](img/stop.jpg)

You don't have to stop the elevator yourself—once it reaches the desired floor the doors will open automatically. However, if you need to stop the elevator mid-way, or change the floor you want to go to, you can do it.

To stop and exit the elevator, press the **Stop** button on the control
panel. The elevator will always stop on the nearest floor and open the
doors.

If you wish to travel to another floor, stop the elevator, but do not leave it. Press the desired floor button and the elevator will take you there.

### Exit

You will again hear a bell upon arrival. Look at the display to make
sure you have arrived on the correct floor. Wait for the doors to open and
exit the elevator. Again, do not worry you will not have enough time to
step out. The elevator will simply halt and wait for you if your
exit takes too long.
