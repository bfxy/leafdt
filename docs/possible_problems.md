# Help

The Elevator's function is maintained only by authorized
professionals. If you have experienced or suspected a faulty operation, please call (555)-ELEVATE and report the problem to the operator. You can also
inform the building administrators or security officers nearby about the
issue.

## <i class="fa fa-wheelchair" aria-hidden="true"></i> Wheelchair

Not a problem. You can use the elevator the same way as anyone else. The doors are wide enough, the space inside is big enough, and the control panel is low enough for you to reach.

However, there is also a special button <i class="fa fa-wheelchair-alt fa-2x" aria-hidden="true"></i> that you can use to call the Elevator. The only difference it makes is the doors will take slightly longer to close.

If are still unsure about traveling alone, don't hesitate to call for an assistant to help you.

## <i class="fa fa-bolt" aria-hidden="true"></i> Emergency

!!! important "Stay calm"

    In any case of emergency **do not panic**. Your safety is ensured at all
    times if you stay calm and follow the simple instructions provided
    below. Panicking may provoke unpredictable behavior that can lead to a
    longer evacuation.

Unfortunately there are situations in which the elevator may suddenly
fail to function properly. In most cases the elevator will stop at the
nearest floor and let the passengers out. However, the elevator may also
stuck between the floors. Such situation requires an assisted
[evacuation](#evacuation) performed by a team of professionals.

### <i class="fa fa-bell" aria-hidden="true"></i> Alarm

In case of a fire or an earthquake alarm, the elevator will
automatically switch to the emergency mode that commands it to stop on
the nearest floor and to open the doors. At this point, each passenger
is required to abandon the carriage. The elevator will render out of
operation.

Follow general evacuation instructions to exit the building.

### <i class="fa fa-wrench" aria-hidden="true"></i> Breakdown

In case of a fault to the mechanism or the electronic control system,
the elevator will try to switch to the emergency mode. However, it is
possible that the blocking mechanism will be set on and the elevator
will stuck.

Stay calm and try to reach the operator over a speaker and call for the
assisted evacuation. If you are stuck inside with a company, try to calm
down everyone who shows signs of panic or visible disturbance. Stay
positive and encourage people into conversations; be ironic about the
situation or tell a funny story. This can cool off the stress and help to
pass the time.

Follow the required procedure to call for help and await
assistance.

### <i class="fa fa-eject" aria-hidden="true"></i> Evacuation

Do not attempt to exit the carriage or climb inside a shaft. The
carriage is the safest place to stay at any time. Wait for the
evacuation team to arrive. Assisted evacuation is only performed by
qualified public-safety professionals who will pull you out safe and
sound. Be open to communicate with the evacuation team as your actions
may be also required.

Remember, patience is a virtue.

## <i class="fa fa-medkit" aria-hidden="true"></i> Health issues

An elevator ride almost never causes serious motion sickness or related
symptoms. However, a full-height travel up a skyscraper involves
undergoing dramatic acceleration, and some symptoms may emerge in
inexperienced passengers. In this case, hit the **Stop** button and exit
the elevator.

Do not be ashamed about the situation. Have a short
break and continue the ride. The symptoms will likely wear off in a
couple of minutes. Remember, your body needs some time to adjust to the
new experience. Take a couple more rides when you start feeling fine, it may help the body to adapt faster.
